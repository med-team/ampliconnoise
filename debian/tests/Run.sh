#!/bin/bash

set -e

nodes=2                              #no. of cluster nodes to use

file=$1; #first argument name of dat file
defaultPrimer="ATTAGATACCC[ACTG]GGTAG"
primer=${2:-$defaultPrimer}

# Arguments passing vary between openmpi and mpich, so attempt to adjust
# depending on the possible implementations.  openmpi notably attempts
# to parse the target command arguments, and mpich will not take the
# end-of-arguments flag, so the situation is not ideal.
if [ "$(readlink -f /usr/bin/mpirun)" = "/usr/bin/mpirun.openmpi" ]
then mpiextra="$mpiextra --"
fi

echo $file
stub=${file%.dat}

echo "Calculating .fdist file"

mpirun $mpiextra PyroDist -in $file -out ${stub} > ${stub}.fout

echo "Clustering .fdist file"

FCluster -in ${stub}.fdist -out ${stub}_X > ${stub}.fout

rm ${stub}.fdist
rm ${stub}_X.otu ${stub}_X.tree
echo "Running PyroNoise"

mpirun $mpiextra PyroNoiseM -din ${file} -out ${stub}_s60_c01 -lin ${stub}_X.list -s 60.0 -c 0.01 > ${stub}_s60_c01.pout

/usr/share/ampliconnoise/Scripts/Truncate.pl 220 < ${stub}_s60_c01_cd.fa > ${stub}_s60_c01_T220.fa

echo "Running SeqDist"
mpirun $mpiextra SeqDist -in ${stub}_s60_c01_T220.fa > ${stub}_s60_c01_T220.seqdist

FCluster -in ${stub}_s60_c01_T220.seqdist -out ${stub}_s60_c01_T220_S > ${stub}_s60_c01_T220.fcout

echo "Running SeqNoise"
mpirun $mpiextra SeqNoise -in ${stub}_s60_c01_T220.fa -din ${stub}_s60_c01_T220.seqdist -lin ${stub}_s60_c01_T220_S.list -out ${stub}_s60_c01_T220_s30_c08 -s 30.0 -c 0.08 -min ${stub}_s60_c01.mapping > ${stub}_s60_c01_T220.snout

rm ${stub}_s60_c01_T220_S.otu ${stub}_s60_c01_T220_S.tree ${stub}_s60_c01_T220.seqdist

echo "Remove degenerate primers"
sed 's/^${primer}//' ${stub}_s60_c01_T220_s30_c08_cd.fa > ${stub}_s60_c01_T220_s30_c08_P.fa
echo "Clustering OTUs"
mpirun $mpiextra NDist -i -in ${stub}_s60_c01_T220_s30_c08_P.fa > ${stub}_s60_c01_T220_s30_c08_P.ndist
  
FCluster -i -in ${stub}_s60_c01_T220_s30_c08_P.ndist -out ${stub}_s60_c01_T220_s30_c08_P > ${stub}_s60_c01_T220_s30_c08_P.fcout

rm ${stub}_s60_c01_T220_s30_c08_P.ndist

echo "Removing intermediate files"
rm *out
exit 0
