#!/usr/bin/make -f
# -*- makefile -*-
#
# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

include /usr/share/dpkg/architecture.mk

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

# Compensate test result mismatches occurring on i386 only.
ifeq ($(DEB_HOST_ARCH),i386)
export DEB_CFLAGS_MAINT_APPEND=-ffloat-store
export DEB_CXXFLAGS_MAINT_APPEND=-ffloat-store
endif

%:
	dh $@

override_dh_auto_install:
	make install
	dh_install bin/* /usr/bin
	dh_install Data/* /usr/share/ampliconnoise/Data/
	dh_install Scripts/* /usr/share/ampliconnoise/Scripts/

#A typo in the upstream Makefile prevents proper cleaning.
override_dh_clean:
	dh_clean
	rm -f *.o
	rm -f *.1

override_dh_installman:
	debian/ampliconnoise.pod
	dh_installman ampliconnoise.1
	( for f in bin/* ; do \
	  ln -s ampliconnoise.1 debian/ampliconnoise/usr/share/man/man1/`basename "$$f"`.1 ; \
	done )

override_dh_compress:
	# Use xz compression for data file to shrink size even more
	find debian -name C005.dat -exec xz \{\} \;
	dh_compress --exclude=.pdf
